#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TH1D.h>
#include <sstream>
#include "Extractor.h"

using namespace std;
// statics
string Extractor::m_channel = "";
DFParser Extractor::s_parser;

// Function find in a string
bool strFind(string s, string f){
    return (s.find(f) != string::npos);
}

/************************************************************
  Constructor
 *************************************************************/    
Extractor::Extractor(string file) : m_nEntries(0),
    m_runNumber(-999),
    m_wEntries(-999),
    m_process(""),
    m_treeName(""),
    m_sys(""),
    m_isMini(false),
    m_isNeg(false),
    m_isAF(false),
    m_noXsec(false),
    m_runSet(false),
    m_processSet(false),
    m_entriesSet(false),
    m_isAFSet(false),
    m_txtName(file),
    m_hforMessage("")
{

    // Initialize SampleXSecition
    m_xSec = new SampleXsection();
    vector<string> xsec_file;
    //xsec_file.push_back("AMIXSection-MC11.data");
    xsec_file.push_back("XSection-MC12-8TeV-4gt.data");
    xsec_file.push_back("XSection-MC12-8TeV-Higgs.data");
    xsec_file.push_back("XSection-MC12-8TeV.data");
    for (int input = 0; input < xsec_file.size(); input++){
        string temp = "../data/";
        temp += xsec_file[input];
        if (!m_xSec->readFromFile(temp.c_str())) 
        {
            cout<<  "Unable to open AMI X-section file '" << temp <<  "' !"<<  endl;
        }
    }

    m_txtFile.open(file.c_str());
    m_treeName = s_parser.getNtuple().name;
    m_isMini = s_parser.isMini();
}

Extractor::~Extractor(){
    m_txtFile.close();
}

void Extractor::setRunNumber(){

    // Extract the run number using parser information
    size_t before, after;
    string before_tag = s_parser.getRunMarkers().before;

    before = m_txtName.find(before_tag) + before_tag.length();
    after = m_txtName.find_first_of(".",before+1);

    string RN = m_txtName.substr(before,after-before);
    m_runNumber = atoi(RN.c_str());

    if (m_runNumber == 0) {
        cout<<"ERROR: File: "<<m_txtName<<" don't match the tags specified in the xml configuration file. RunNumber not found."<<endl;
        exit(1);
    }

    m_runSet = true;
}

void Extractor::setProcess(){

    // Extract process from the file name
    size_t before, after;
    string before_tag = s_parser.getProcMarkers().before;
    string after_tag = s_parser.getProcMarkers().after;

    if (before_tag.length() == 0){
        string rn_before = s_parser.getRunMarkers().before;
        before = m_txtName.find_first_of(".",m_txtName.find(rn_before) + rn_before.length()) + 1;
        after = m_txtName.find(after_tag);
    } else {
        before = m_txtName.find_first_of(".",m_txtName.find(before_tag) + before_tag.length()) + 1;
        after = m_txtName.find(after_tag);
    }

    m_process = m_txtName.substr(before,after - before);

    // Check that process has been well readed
    if (m_process.length() == 0) {
        cout<<"ERROR: File: "<<m_txtName<<" don't match the tags specified in the xml configuration file. Process name not found."<<endl;
        exit(1);
    }

    // Define the systematic based on name
    // If T1 is not 105200 add the run number at the end
    stringstream rnstream;
    if (runNumber() != 105200) rnstream<<"_"<<runNumber();
    else rnstream<<""; 
    if (strFind(m_process,"T1_")) m_process = m_process+rnstream.str();


    //Create LumSys for each file
    m_sys.clear();
    //Z+jets
    if ( strFind(m_process,"AlpgenJimmyZ") ) m_sys = "*LumSysZj";
    // W+jets
    if ( strFind(m_process,"AlpgenJimmyW") || strFind(m_process,"AlpgenW")  ) m_sys = "*LumSysWj";
    // TTbar
    if ( strFind(m_process,"T1_")       || strFind(m_process,"protos_ttbar")|| 
            strFind(m_process,"TTbar_")    || strFind(m_process,"AcerMCttbar") ||
            strFind(m_process,"toprex_tt_")|| strFind(m_process,"AlpgenJimmyttbar") ) m_sys = "*Lumm_syst";
    // Diboson
    if ( strFind(m_process,"_Herwig")       || strFind(m_process,"AlpgenJimmyWW") || 
            strFind(m_process,"AlpgenJimmyWZ") || strFind(m_process,"AlpgenJimmyZZ") || 
            strFind(m_process,"McAtNlo_JIMMY_") ) m_sys = "*LumSysWZp";
    // SingleTop
    if ( strFind(m_process,"st_") ) m_sys = "*LumSysst";

    // Set m_isNeg to its correct value
    if ( (strFind(m_process,"McAtNlo") || strFind(m_process,"st_")) && !strFind(m_process,"PowHeg") ) m_isNeg = true;
    if (m_isMini) m_isNeg = true;

    // Add AF label
    if (isAF()) m_process += "_atlasfast";


    // Substitutions based on process runs
    SubsCont subs = s_parser.getProcSubs();
    for (int i = 0; i < subs.size(); i++){
        if (runNumber() == atoi(subs[i].id.c_str())) m_process = m_process + "_" + subs[i].suffix;
    }

    m_processSet = true;
}

void Extractor::setIsAF(){

    size_t before, third;

    string after_proc = s_parser.getProcMarkers().after;
    before = m_txtName.find(after_proc) + after_proc.length();
    third = m_txtName.find_first_of("_",m_txtName.find_first_of("_",before + 1) + 1);

    m_isAF =  m_txtName.substr(before,third - before).find("a") != string::npos;

    m_isAFSet = true;
}

double Extractor::weightedEntries(TFile *tfile){
    // This function is used to calculate the number of entries with +1/-1 weights
    // in the case it is needed.

    double weighted_entries = 0;

    if (!m_isMini){

        TTree *tree = (TTree*)tfile->Get(m_treeName.c_str());
        long long int entries = tree->GetEntries();
        vector<vector<double> > *weight;
        weight = 0;
        TBranch *b_weight;
        tree->SetBranchAddress("mcevt_weight", &weight, &b_weight);
        for (long long int i = 0; i < entries; i++)
        {
            tree->GetEntry(i);
            weighted_entries += (*weight)[0][0];
        }
    }
    else if (m_isMini){
        string base_histo  = s_parser.getNtuple().baseHisto;
        int base_histo_bin = s_parser.getNtuple().baseHistoBin;
        string hforname = s_parser.getNtuple().hforHisto;
        TH1D *histo = (TH1D*)tfile->Get(base_histo.c_str());
        TH1D *hfor;
        if (hforname != "") hfor= (TH1D*)tfile->Get(hforname.c_str());
        if (!histo){
            cerr<<"ERROR: No histogram "<<base_histo<<" could be found for normalization. Check the config file and make sure the name is well defined."<<endl;
            cerr<<"Exiting execution..."<<endl;
            exit(1);
        }
        weighted_entries = histo->GetBinContent(base_histo_bin);
        if (!hfor && hforname != ""){
            cout<<"ERROR: No histogram named "<<hforname<<" could be found for hfor cut. No hfor scale will be applied. Check the config file and make sure the name is well defined."<<endl;
            cout<<"TFile: "<<tfile->GetName()<<endl;
            delete histo;
            delete hfor;
            return weighted_entries;
        }else if(hforname == ""){
            delete histo;
            return weighted_entries;
        }else{
            stringstream tmp;
            double init = hfor->GetBinContent(1);
            double final = hfor->GetBinContent(2);
            if (init != final){
                tmp<<"Process: "<<process()<<"\n\t Initial = "<<init<<", passed = "<<final;
                m_hforMessage =tmp.str();
            }
            weighted_entries *= init/final;
        }

        delete histo;
        delete hfor;

    }

    return weighted_entries;
}

bool Extractor::goodNtuple(string ntu){

    TFile* ntuple = new TFile(ntu.c_str(), "READ");
    if (!ntuple->IsZombie()) {
        ntuple->Close();
        delete ntuple;
        return true;
    }

    ntuple->Close();
    delete ntuple;
    m_zombies.push_back(ntu);
    return false;

}

void Extractor::setEntries(){
    //This function derive the number of entries for each RunNumber
    //adding the number of entries in each ntuple on the txt. Depending
    // on the IsNeg variable it chooses to use the +1/-1 weights or
    // getnetry.

    double entries = 0;
    double wentries = 0;
    int count = 0;

    while (!m_txtFile.eof())
    {
        string ntu;
        m_txtFile>>ntu;
        if ( ntu.size() > 1 && goodNtuple(ntu)) //Protection for files with empty line and Avoid broken ntuples
        {

            TFile* ntuple = new TFile(ntu.c_str(), "READ"); 
            //Check if the TFile has been opened 
            if (!(ntuple->IsOpen())) {
                cout<<"============================="<<endl;
                cout<<"!WARNING"<<endl;
                cout<<"The TFile could not be opened"<<endl;
                cout<<"============================="<<endl;
            }
            count++;

            //string currentfile = ntuple->GetName();
            //cout<<currentfile<<endl;
            if (m_isMini){
                string base_histo  = s_parser.getNtuple().baseHisto;
                int base_histo_bin = s_parser.getNtuple().baseHistoBin;
                TH1D *noMcWeight = (TH1D*)ntuple->Get(base_histo.c_str());
                if(!noMcWeight){
                    cout<<"Could not get histogram from "<<ntu<<endl;
                    ntuple->Close();
                    delete ntuple;
                    continue;
                }
                double wmcev = noMcWeight->GetBinContent(2)/noMcWeight->GetBinContent(1);
                if (wmcev == 0){
                    cout<<"Sample with zero mcevt_weigth in the mini ntuple. Skipping sample."<<endl;
                    ntuple->Close();
                    delete ntuple;
                    continue;
                }
                entries += noMcWeight->GetBinContent(base_histo_bin)/wmcev; 
                delete noMcWeight;
            } else {
                TTree *tree = (TTree*)ntuple->Get(m_treeName.c_str());
                entries += tree->GetEntries();
                delete tree;
            }

            if (m_isNeg || m_isMini) {
                wentries += weightedEntries(ntuple);
            }
            ntuple->Close();
            delete ntuple;
        }
        //ntuple->Close();
    }
    m_nEntries = entries;
    m_wEntries = wentries;

    m_entriesSet = true;
    m_isEmpty = (count == 0) ? true : false;
}

double Extractor::xSec_SF(){
    double xs = m_xSec->getXsection(runNumber()); 
    m_noXsec = xs < 0;

    return xs; 
}

/*
   string Extractor::Process()
   {

   system("rm pr");
   string exe = "echo \""+File+"\" | awk -F. '{print $5}' >> pr";

   system(exe.c_str());

   fstream fProcess("pr");
   string process;
   fProcess>>process;        

   fProcess.close();

//Create Syst for each file
Syst.clear();
if ( strstr(process.c_str(), "AlpgenJimmyZ") ) Syst = "*LumSysZj";
if ( strstr(process.c_str(), "AlpgenJimmyW") || strstr(process.c_str(), "AlpgenW")  ) Syst = "*LumSysWj";
if ( strstr(process.c_str(), "protos_ttbar") || strstr(process.c_str(), "TTbar_") || strstr(process.c_str(), "AcerMCttbar") || strstr(process.c_str(), "toprex_tt_") || strstr(process.c_str(), "AlpgenJimmyttbar") ) Syst = "*LumSystt";
if ( strstr(process.c_str(), "_Herwig") || strstr(process.c_str(), "AlpgenJimmyWW") || strstr(process.c_str(), "AlpgenJimmyWZ") || strstr(process.c_str(), "AlpgenJimmyZZ") || strstr(process.c_str(), "McAtNlo_JIMMY_") ) Syst = "*LumSysWZp";
if ( strstr(process.c_str(), "st_") ) Syst = "*LumSysst";


return process;
}
*/

void Extractor::setXMLFile(const char* _file){
    // Parse XML config file
    int value = s_parser.parseFile(_file);

    if (value != 0){
        cerr<<"Error reading XML File. Make sure that it exists and is well defined"<<endl;
        exit(1);
    }
}
