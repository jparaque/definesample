/*
 * Parser for configuartion XML file used for DefineSamples tool.
 */

#include "DFParser.h"
#include "tinyxml2.h"

using  std::cout;
using  std::cerr;
using  std::endl;
using  std::vector;
using  std::string;
using namespace tinyxml2;

// Constructor
DFParser::DFParser(){
    m_sample_path = "";
    m_sample_id = "";
    m_sample_version = "";
    m_run_markers.before = "";
    m_run_markers.after = "";
    m_proc_markers.after = "";
    m_proc_markers.before = "";
    m_tag_subs.clear();
    m_proc_subs.clear();
    m_d3pd.name = "";
    m_d3pd.baseHisto = "";
    m_d3pd.baseHistoBin = -1;
    m_mini.name = "";
    m_mini.baseHisto = "";
    m_mini.baseHistoBin = -1;
}

// Destructror
DFParser::~DFParser(){
}

// Parse the file and define variables
int DFParser::parseFile(const char* file){

    //CReate document
    XMLDocument doc;
    doc.LoadFile(file);
    if (doc.ErrorID() != 0) return doc.ErrorID();

    // Start scanning the file
    XMLElement *sample = doc.FirstChildElement("Sample");
    if (!sample) {
        cout<<"Error while loading file. No Sample found."<<endl;
        return -1;
    }
    m_sample_path = sample->Attribute("Path");
    m_sample_id = sample->Attribute("Identifier");
    m_sample_version = sample->Attribute("Version");
    m_sample_suffix= sample->Attribute("Suffix");

    XMLElement *elem = sample->FirstChildElement();
    while(elem){
        string name = elem->Name();

        // Read markers
        if (name == "Markers"){
            XMLElement *marker = elem->FirstChildElement();
            while(marker){
                string markername = marker->Name();
                if (markername == "RunNumber"){
                    m_run_markers.before = marker->Attribute("Before");
                    m_run_markers.after = marker->Attribute("After");
                } else if (markername == "Process"){
                    m_proc_markers.before = marker->Attribute("Before");
                    m_proc_markers.after =  marker->Attribute("After"); 
                }
                marker = marker->NextSiblingElement();
            }
        }

        // Read substitutions
        if (name == "Substitutions"){
            XMLElement *sub = elem->FirstChildElement();
            while(sub){
                string subname = sub->Name();
                if (subname == "Tag"){
                    Subs tag;
                    tag.id = sub->Attribute("Identifier");
                    tag.suffix = sub->Attribute("Suffix");
                    m_tag_subs.push_back(tag);
                }
                if (subname == "Process"){
                    Subs proc;
                    proc.id = sub->Attribute("Identifier");
                    proc.suffix = sub->Attribute("Suffix");
                    m_proc_subs.push_back(proc);
                }
                sub = sub->NextSiblingElement();
            }
        }

        // Read ntuple information
        if (name == "Ntuple"){
            m_type = elem->Attribute("Type");
            if (m_type == "D3PD" || m_type == "d3pd"){
                m_ntuptype = D3PD;
                m_d3pd.name = elem->Attribute("TreeName");
                if (m_d3pd.name == "") {
                    cout<<"Erro: No TreeName supplied."<<endl;
                    return -1;
                }
            }
            else if (m_type == "Mini" || m_type == "mini" || m_type == "MINI"){
                m_mini.name = elem->Attribute("TreeName");
                m_ntuptype = MINI;
                if (m_mini.name == "") {
                    cout<<"Erro: No TreeName supplied."<<endl;
                    return -1;
                }
                m_mini.baseHisto = elem->Attribute("BaseHisto");
                int error = elem->QueryIntAttribute("BaseHistoBin", &(m_mini.baseHistoBin));
                if (error == XML_WRONG_ATTRIBUTE_TYPE) {
                    cout<<"Error converting base histo bin into an integer"<<endl;
                    return -1;
                } else if(error == XML_NO_ATTRIBUTE){
                    cout<<"Error reading base histogram bin. No attribute found named BaseHistoBin in the Ntuple field"<<endl;
                    return -1;
                }
                if (elem->Attribute("HFORHisto") != NULL)
                m_mini.hforHisto = elem->Attribute("HFORHisto");
            } else {
                cout<<"Error while reading the Ntuple information: No valid type. Please choose D3PD or Mini"<<endl;
                return -1;
            }
        }
        elem = elem->NextSiblingElement();
    }

    // Check that all parameters are well defined and printout information
    return printOut();
}


/*
 * Check that all needed values are well defined
 */
int DFParser::printOut(){

    if (m_sample_path == "" ||
        m_sample_id == "") {
            cout<<"Error in sample definition: Path and identifier are needed and not parsed"<<endl;
            return -1;
    }
    if (m_run_markers.before == "" ||
        m_proc_markers.after == ""){
        cout<<"Error in markers definition. The text tag before the run number and after the process are needed."<<endl;
        cout<<"No supplying them may cause bad process or RunNumber identification"<<endl;
        return -1;
    }
    if (m_ntuptype == MINI){
        if (m_mini.baseHisto == "" ||
            m_mini.baseHistoBin == -1){
            cout<<"Error in mini ntuple definition: No base histogram or bin has been defined."<<endl;
            return -1;
        }
    }

    // Print out information
    cout<<"**************************************************"<<endl;
    cout<<"Sample:"<<endl;
    cout<<"--------------------------------------------------"<<endl;
    cout<<"Path         : "<<m_sample_path<<endl;
    cout<<"Indentifier  : "<<m_sample_id<<endl;
    if (m_sample_version != "")
    cout<<"Version      : "<<m_sample_version<<endl;
    if (m_sample_suffix != "")
    cout<<"Suffix       : "<<m_sample_suffix<<endl;
    cout<<"**************************************************"<<endl;
    cout<<"Ntuple:"<<endl;
    cout<<"--------------------------------------------------"<<endl;
    cout<<"Type           : "<<m_type<<endl;
    if (m_ntuptype == MINI){
    cout<<"Tree name      : "<<m_mini.name<<endl;
    cout<<"Base histogram : "<<m_mini.baseHisto<<endl;
    cout<<"Base histo bin : "<<m_mini.baseHistoBin<<endl;
    if(m_mini.hforHisto == "")
    cout<<"HFOR histogram : No HFOR histogram supplied. HFOR > 3 events are not included by default. Make suere this is what you want."<<endl;
    else
    cout<<"HFOR histogram :"<<m_mini.hforHisto<<endl;
    } else {
    cout<<"Tree name :"<<m_d3pd.name<<endl;
    }
    cout<<"Markers     | RunNumber\t\t\t| Process"<<endl;
    cout<<"--------------------------------------------------"<<endl;
    cout<<"Before      |"<<m_run_markers.before<<"\t\t\t|"<<m_proc_markers.before<<endl;
    cout<<"After       |"<<m_run_markers.after<<"\t\t\t|"<<m_proc_markers.after<<endl;
    cout<<"**************************************************"<<endl;
    cout<<"Substitutions: "<<endl;
    cout<<"--------------------------------------------------"<<endl;
    if(m_proc_subs.size() > 0){
    cout<<"Based on process name:"<<endl;
    cout<<"Identifier \t\t | Suffix"<<endl;
    for(int i = 0; i < m_proc_subs.size(); i++){
        cout<<m_proc_subs[i].id<<"\t\t | "<<m_proc_subs[i].suffix<<endl;
    }
    }
    if (m_tag_subs.size() > 0){
    cout<<"Based on tags:"<<endl;
    cout<<"Identifier \t\t | Suffix"<<endl;
    for(int i = 0; i < m_tag_subs.size(); i++){
        cout<<m_tag_subs[i].id<<"\t\t | "<<m_tag_subs[i].suffix<<endl;
    }
    }
    cout<<"--------------------------------------------------"<<endl;

    return 0;
}
