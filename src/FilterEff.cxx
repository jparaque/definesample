#include "FilterEff.h"

using namespace std;

/*
 * @brief Default constructor
 */
FilterEff::FilterEff(){
}

/*
 * @brief Default desctructor
 */
FilterEff::~FilterEff(){

}

/*
 * @brief Set file to read efficiencies from
 */
void FilterEff::setFile(string _file){
    // Open file
    m_effFile.open(_file.c_str());

    // Load efficiencies in the efficiencies map
    while (!m_effFile.eof()){
        int run;
        m_effFile>>run;
        double eff;
        m_effFile>>eff;
        if (run > 0) m_efficiency[run] = eff;
    }

}

/*
 * @brief Get the efficiency of a given run
 */
double FilterEff::getEff(int run){

    if (m_efficiency.find(run) == m_efficiency.end()) return 1;
   
    return m_efficiency[run];

}
