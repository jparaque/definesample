#!/usr/bin/python
import commands, os, glob,sys
from optparse import OptionParser

gPaths = ""
gPattern = ""


#-------------------------------------------------
def getSystematics():
    bpos = gPattern.rfind("syst")
    apos = bpos + 4
    #print bpos
    #print apos
    #print gPattern[:bpos]
    #print gPattern[apos:]
    before = gPattern[:bpos]
    after = gPattern[apos:]
    syst_list = []
    paths = glob.glob(gPaths)
    for folder in paths:
        if os.path.isdir(folder) and before in folder and after in folder: 
            #print folder
            #print after
            apos = folder.rfind(after)
            bpos = folder.rfind(before)+len(before)
            #print "aposnew: "+str(apos)
            #print "bposnew: "+str(bpos)
            #print folder
            #print folder[:bpos]
            #print folder[apos:]
            #print folder[folder[:bpos].rfind("/")+bpos:apos]
            #print folder[bpos:apos]
            syst_list.append(folder[bpos:apos])
    for s in range(len(syst_list)):
        if syst_list[s] == "":
            syst_list[s] = "nominal"
    return set(syst_list)


if __name__ == "__main__":

    #______ Option parser __________________________________________
    parser = OptionParser()
    parser.add_option("-s","--syst",
            help="Use this option if you want to run the script to cretae the list of systematics to use. You need to supply also the -i and -p option. Read below.",
            action="store_true",
            dest = "doSyst",
            default=False)
    parser.add_option("-i","--input",
            help="Folders where your samples lists are stored. This is only used to find all systematics, the definition of the samples you want to use is done inside the xml file. Default: input/mc12/list_*",
            dest="paths",
            default="input/mc12/list_*")
    parser.add_option("-p","--pattern",
            help="Pattern of your path names to identify systematics names, i.e. list_syst. This example will search for any folder uner the path with a name which is list_syst, and take syst as a wildcard that will be changed for each systematic name. In the case of JES, for example, with the option -p \"list_syst\" it will recognize list_JES as the folder with the lists of JES systematic. default: list_syst",
            dest="pattern",
            default="list_syst")
    parser.add_option("-r","--runningMode",
            help="Running mode to launch the tool: local, cluster. \n If you choose cluster you might need to supply the queue name (-q option). default: cluster",
            dest="run",
            default="cluster")
    parser.add_option("-q","--queue",
            help="Queue to use when running the code in cluster mode",
            dest="queue",
            default="")
    parser.add_option("-f","--files",
            help="Configuration xml files you want to use as a template. Add them between \'\"\'. i.e. \"ele.xml muon.xml\" ",
            dest="files",
            default="")

    (options,args) = parser.parse_args()

    if not options.files and not options.doSyst:
        parser.error("I didn't see the -f option with the xml files you want to use.")

    gPaths = options.paths
    gPattern = options.pattern
    run = options.run
    queue = options.queue
    if queue != "":
        queue = "-q "+queue
    configs = options.files.split()
    doSyst = options.doSyst

    print "Running for xml files: "
    for file in configs:
        print "\t "+file
    print "Running mode:"
    print "\t "+run

    #_______________________________________________________________

    rm = "rm nohup*"
    commands.getstatusoutput(rm)
    if doSyst:
        print "Using paths:   "+gPaths
        print "Using pattern: "+gPattern
        syst_list = getSystematics()        
        fileSyst = open("../samples/systList.txt","w")
        for syst in syst_list:
            print "Added:"+syst
            fileSyst.write(syst+"\n")
        sys.exit(0)
    
    syst_file = open("../samples/systList.txt","r")
    syst_list = syst_file.read().splitlines()
    print "Running for systematis: "
    for s in syst_list:
        print "\t "+s
    # Launch all jobs
    for systematic in syst_list:
        launch = []
        if systematic == "nominal":
            print "Creating samples files for nominal"
            syst = "nominal"
            for file in configs:
                newfile = file.replace(".xml","")
                exe = "Change.sh "+file+" "+newfile+"_nominal.xml  @syst@ "
                commands.getstatusoutput(exe)
            if run == "local":
                exe = ""
                for file in configs:
                    newfile = file.replace(".xml","")
                    exe += "CreateDS -f "+newfile+"_nominal.xml > Outmm"+syst+";"
                os.system(exe)
            else:
                script = open("submit_nominal.sh","w")
                script.write("#!/bin/bash\n")
                script.write("#PBS -l walltime=00:20:00\n")
                script.write("cd $PBS_O_WORKDIR\n")
                for file in configs:
                    newfile = file.replace(".xml","")
                    script.write("CreateDS -f "+newfile+"_nominal.xml > Outmm"+syst+"\n")
                script.close()
                os.system("chmod +x submit_nominal.sh")
                os.system("qsub "+queue+" submit_nominal.sh")
        else:
            syst = "_"+systematic
            print "Creating samples files for",systematic
            for file in configs:
                newfile = file.replace(".xml","")
                exe = "Change.sh "+file+" "+newfile+syst+".xml  @syst@ "+syst
                commands.getstatusoutput(exe)
            if run == "local":
                exe = ""
                for file in configs:
                    newfile = file.replace(".xml","")
                    exe += "CreateDS -f "+newfile+syst+".xml > Outmm"+syst+";"
                os.system(exe)
            else:
                file = "submit"+syst+".sh"
                script = open(file,"w")
                script.write("#!/bin/bash\n")
                script.write("#PBS -l walltime=00:20:00\n")
                script.write("cd $PBS_O_WORKDIR\n")
                for cfile in configs:
                    newfile = cfile.replace(".xml","")
                    script.write("CreateDS -f "+newfile+syst+".xml > Out"+newfile+syst+"\n")
                script.close()
                os.system("chmod +x "+file)
                os.system("qsub "+queue+" "+file)
    print "Submitted "+str(len(syst_list))+" jobs"
        #for exe in launch:
            #os.system(exe)
