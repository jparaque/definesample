#!/bin/sh
#for file in *_Stat.txt

INFILE=$1
OUTFILE=$2
TOCHANGE=$3
CHANGETO=$4

echo "1: "$1
echo "2: "$2
echo "3: "$3
echo "4: "$4

sed -e "s/$TOCHANGE/$CHANGETO/g" "$INFILE" > "$INFILE".tmp && mv -f "$INFILE".tmp "$OUTFILE"

