/************************************************************
  Code for create de define samples for MC processes from the 
  txt files where the list of root files path are.

  The structure of the txt files need to be as follows:
  After the "TeV." tag in the name there should be the next fields
  separated by dots

  1) RunNumber
  2) Process name
  3) "Merge"
  4) "NTUP_Top"
  5) Grid tags 
  6) "txt"
  TODO:Update description
 *************************************************************/	
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <math.h>
#include "TopDataPreparation/SampleXsection.h"
#include "FilterEff.h"
#include "Extractor.h"
#include "DFParser.h"
#include "TopAnalysisBase/CmdLineSvc.h"

using namespace std;


/************************************************************
Function used to select Sherpa Z+jets samples
*************************************************************/    
bool isSherpaZjets(int run){
    return (run == 147770 ||    run == 147771 ||    run == 147772 ||   
        run == 147773 ||    run == 167749 ||    run == 167750 ||   
        run == 167751 ||    run == 167752 ||    run == 167753 ||   
        run == 167754 ||    run == 167755 ||    run == 167756 ||          
        run == 167757 ||    run == 167758 ||    run == 167759 ||           
        run == 167760 ||    run == 167797 ||    run == 167798 ||    
        run == 167799 ||    run == 167800 ||    run == 167801 ||    
        run == 167802 ||    run == 167803 ||    run == 167804 ||    
        run == 167805 ||    run == 167806 ||    run == 167807 ||    
        run == 167808 ||    run == 167809 ||    run == 167810 ||    
        run == 167811 ||    run == 167812 ||    run == 167813 ||    
        run == 167814 ||    run == 167815 ||    run == 167816 ||    
        run == 167817 ||    run == 167818 ||    run == 167819 ||    
        run == 167820 ||    run == 167821 ||    run == 167822 ||    
        run == 167823 ||    run == 167824 ||    run == 167825 ||    
        run == 167826 ||    run == 167827 ||    run == 167828 ||    
        run == 167829 ||    run == 167830 ||    run == 167831 ||    
        run == 167832 ||    run == 167833 ||    run == 167834 ||    
        run == 167835 ||    run == 167838 ||    run == 167839 ||    
        run == 167840 ||    run == 167841 ||    run == 167842 ||    
        run == 167843 ||    run == 167844);
}


/************************************************************
Usage function
*************************************************************/    
void usage(){
    cout<<"CreateDS is used to create the files needed for inputs definition for Lip Analysis tools."<<endl;
    cout<<"Usage: CreateDS [option]"<<endl;
    cout<<"-f      This option is mandatory and specify what will be the xml configuration file used to generate the define samples. "<<endl;
    cout<<"        Check the wiki https://bitbucket.org/jparaque/definesample/wiki/Home for more information about how to create this file."<<endl;
    cout<<"-old    This option is used if you want to generate the the ouput files as it was done for previous versions of the analysis (i.e. a file with lots of \"if\" statements selecting each sample)."<<endl;
    cout<<"        By default the new configuration will be run (i.e. txt configuration files with the information needed for each sample to be run)."<<endl;
}


//---------
// Main
// --------
int main(int argc, char *argv[]){



    string xmlfile;
    bool old_version = false;

    /************************************************************
    Read parameteres and print help
    *************************************************************/    
    CmdLineSvc *cmdLine = CmdLineSvc::svc(argc,argv);
    if (cmdLine->hasKey("-h")){
        usage();
        return 0;
    } else {
        if (cmdLine->fillStr("-f",xmlfile) != 0){
            cout<<"ERROR: No xml configuration file has been selected. Plese run with the -h option to get information"<<endl;
            return 1;
        }
        old_version = cmdLine->hasKey("-old");
    }


    // Define parser
    Extractor::setXMLFile(xmlfile.c_str());
    DFParser parser = Extractor::getParser();

    // Time variables
    time_t start, end;
    start = time(0);
    // TODO: CHECK IF EVERY FILE IS WELL OPENED
    //Create a file to read the txt

    // String for the direcotry where it should look for list of txt
    // This should be changed when needed
    string suffix = parser.getSampleSuffix();
    string variables = "DefineSamples_MC_variables_";
    variables = variables + suffix + ".cxx";
    string code = "DefineSamples_MC_code_";
    code = code + suffix + ".cxx";
    string info = "DefineSamples_MC_info_";
    info = info + suffix + ".txt";
    // In case it is specified create a new version of the samples in the samples folder
    string version = parser.getSampleVersion();
    if (version.length() > 0){
        string exe;
        exe = (old_version) ? "mkdir -p ../samples/"+version : "mkdir -p ../samples/"+version+"/infofiles";
        system(exe.c_str());
        variables = "../samples/"+version+"/"+variables;
        code = "../samples/"+version+"/"+code;
        info = "../samples/"+version+"/infofiles/"+info;
    }
    // Redirectin output buffer
    streambuf *oldcout = cout.rdbuf();
    string logoutput = "../samples/"+version+"/"+"Log_";
    logoutput = logoutput+suffix;
    ofstream out(logoutput.c_str());
    cout.rdbuf(out.rdbuf());

    // Copy cross sectino files used to maintain any change if needed
    if (version != ""){
        system(("cp -r ../data ../samples/"+version).c_str());
    }

    cout << endl << "Running with the RootCore on the following path: " << getenv("ROOTCOREDIR") << endl << endl;

    // Get text files with the ntuples path
    string directory = parser.getSamplePath();
    string ID        = parser.getSampleID();
    string exe = "ls "+directory+"/"+ID+" | grep -v \"AlpgenJimmytt\" > files"+suffix;
    system(exe.c_str());
    string tmp_files = "files"+suffix;
    fstream files(tmp_files.c_str());
    if (!files.is_open()){
        cout<<"ERROR LOADING FILES: Check the intput directory and make sure it contains the txt files needed."<<endl;
    }
    //Create a file to save the name of all processes
    ofstream processes(variables.c_str());
    
    // Create file to save all properties
    ofstream txtinfo;
    
    if (!old_version) txtinfo.open(info.c_str());
    
    // Create a file to save selection code
    ofstream DS(code.c_str());
    DS.setf(ios_base::fixed);

    // Track problems
    vector<string> NoXsec;
    vector<int> NoXsec_RN;
    vector<string> EmptyTxt;
    vector<int> EmptyTxt_RN;
    string name;
    vector<string> allProc;
    vector<string> repProc;
    vector<string> hforProc;

    //Write the define samples for all the processes
    while (!files.eof())
    {
        files>>name;
        //Avoid these streams for now. In the future they can be changed
        if (!strstr(name.c_str(), "_Mass") && !strstr(name.c_str(), "AlpgenJimmyWgamma") && name.size() > 1)
        {			
            // Create an objet of Extractor
            Extractor vars(name);	
            cout<<name<<endl;
            //cout<<"Postive and negative weight: "<<vars.IsNeg<<endl;

            // Extract variables for each process	
            string Process = vars.process();
            // Check if there is another one with the same name
            if (find(allProc.begin(), allProc.end(),Process) != allProc.end()) repProc.push_back(Process);
            allProc.push_back(Process);
            //Store in the variables file
            processes<<"int "<<Process<<" = 0;"<<endl; // Declare process

            // Read variables
            int RN = vars.runNumber();

            // Add filter efficiency: Deprecated but kept here in case it is used again
            FilterEff eff;
            if ( argc == 7) eff.setFile(argv[6]);

            double WEntries = vars.wEntries();
            double XSec_SF = vars.xSec_SF();
            stringstream fakeint_entries;
            fakeint_entries<<setprecision(0)<<fixed<<floor(vars.entries()+0.5);
            //XSec_SF *= eff.getEff(RN); 

            if (old_version){
                // Create the define samples code for old version
                DS<<"  if("<<Process<<"){"<<endl;
                DS<<"    ifstream in;"<<endl;
                DS<<"    in.open(\""<<name<<"\");"<<endl;
                DS<<"    while (1) {"<<endl;
                DS<<"      std::string rootfile;"<<endl;
                DS<<"      in >> rootfile; if (!in.good()) break;"<<endl;
                DS<<"      Input.File(ntu, rootfile);"<<endl;
                DS<<"    }"<<endl;
                DS<<"    in.close();"<<endl;
                if (strstr(Process.c_str(), "toprex"))
                {
                    DS<<"    TMonteCarlo mc_"<<RN<<"(2, "<<RN<<", "<<WEntries<<"/1/1000*LumSys"<<vars.syst()<<", "<<fakeint_entries.str()<<", \""<<RN<<"_"<<Process<<"\", MaxCuts);"<<endl;
                }else{	
                    DS<<"    TMonteCarlo mc_"<<RN<<"(1, "<<RN<<", "<<WEntries<<"/"<<XSec_SF<<"/1000*LumSys"<<vars.syst()<<", "<<fakeint_entries.str()<<", \""<<RN<<"_"<<Process<<"\", MaxCuts);"<<endl;		
                }
                DS<<"    MonteCarlo.push_back(mc_"<<RN<<");"<<endl;
                DS<<"  }"<<endl; 
                DS<<endl;
            } else {
                if (strstr(Process.c_str(), "toprex")) {
                    txtinfo << Process.c_str() << " " << name << " 2 " << RN << " " << WEntries << " " << fakeint_entries.str() << endl;
                }else{
                    txtinfo << Process.c_str() << " " << name << " 1 " << RN << " " << WEntries << " " << XSec_SF << " " << fakeint_entries.str() << endl;
                }

                DS << "samples[\"" << Process.c_str() << "\"] = "<< Process.c_str() <<";" << endl;
            }


            // Broken files log	
            if ( vars.nonExistentFiles().size() != 0){
                cout<<"Broken files: "<<endl;
                for (int i = 0; i < vars.nonExistentFiles().size(); i++) {
                    cout<<vars.nonExistentFiles()[i]<<endl;
                }
            }
            if (vars.noXsec()){
                NoXsec.push_back(Process);
                NoXsec_RN.push_back(RN);
            }

            if (vars.isEmpty()){
                EmptyTxt.push_back(Process);
                EmptyTxt_RN.push_back(RN);
            }

            string hfor = vars.hforMessage();
            if (hfor != "") hforProc.push_back(hfor);
        }
        name.clear();
    }

    // Couts
    cout<<endl<<"__________________________________________________"<<endl;
    cout<<"Processes without cross section: "<<endl;
    for (int i = 0; i < NoXsec.size(); i++) {
        cout<<NoXsec_RN[i]<<": "<<NoXsec[i]<<endl;
    }
    cout<<endl<<"__________________________________________________"<<endl;
    cout<<"Empty txt files: "<<endl;
    for (int i = 0; i < EmptyTxt.size(); i++) {
        cout<<EmptyTxt_RN[i]<<": "<<EmptyTxt[i]<<endl;
    }
    cout<<endl<<"__________________________________________________"<<endl;
    cout<<"Processes with repeated names: "<<endl;
    for (int i = 0; i < repProc.size(); i++) {
        cout<<repProc[i]<<endl;
    }
    cout<<endl<<"__________________________________________________"<<endl;
    cout<<"Processes with hfor normalisation used: "<<endl;
    for (int i = 0; i < hforProc.size(); i++) {
        cout<<hforProc[i]<<endl;
    }
    cout<<endl<<"__________________________________________________"<<endl;

    DS.close();
    files.close();
    processes.close();
    if (!old_version) txtinfo.close();

    end = time(0);
    // Print time
    double ittook= end-start;
    double isec, imin, ihour, fsec, fmin;
    fsec = modf(ittook/60, &imin);	
    fmin = modf(imin/60, &ihour);

    int sec = (int)(fsec*60 + 0.5);
    int min = (int)imin + (int)(fmin*60 + 0.5);

    //      double min = (difftime(tend, tstart)/60)-hour*60;
    //      double sec = (difftime(tend, tstart))-min*60-hour*3600;


    // Remove temporary files file
    /*exe = "rm files";
      system(exe.c_str());*/
    cout<<endl<<"---> Time <---"<<endl;
    cout << "The define sample was created in "<< ihour <<":" << min << ":" << sec << " (hh:min:sec)"<<endl;

    // Reset cout
    cout.rdbuf(oldcout);
    out.close();

    return 0;
}
