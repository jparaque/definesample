#ifndef EXTRACTOR_H
#define EXTRACTOR_H
// Miniclass that extract the variables needed to save in the define samples
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "DFParser.h"
#include "TopDataPreparation/SampleXsection.h"

class TFile;

class Extractor{
    private:
        double weightedEntries(TFile*);
        void setEntries();
        bool goodNtuple(std::string);
        void setRunNumber();
        void setProcess();
        void setIsAF();

        static std::string m_channel;
        SampleXsection *m_xSec;
        static DFParser s_parser;
        double m_nEntries;
        double m_wEntries;
        int m_runNumber;
        std::string m_process;
        std::string m_treeName;
        std::string m_sys;
        std::ifstream  m_txtFile;
        std::string m_txtName;
        std::string m_hforMessage;
        std::vector<std::string> m_zombies;

        bool m_isNeg;
        bool m_isMini;
        bool m_isAF;
        bool m_noXsec;
        bool m_runSet;
        bool m_processSet;
        bool m_entriesSet;
        bool m_isAFSet;
        bool m_noXsecSet;
        bool m_isEmpty;

    public:

        int runNumber(){if(!m_runSet) setRunNumber();return m_runNumber;};
        std::string process(){if(!m_processSet) setProcess();return m_process;};
        std::string syst(){if(!m_processSet) setProcess(); return m_sys;};
        double entries(){if(!m_entriesSet) setEntries();return m_nEntries;};
        double wEntries(){if(!m_entriesSet) setEntries();return m_wEntries;};
        std::vector<std::string> nonExistentFiles(){return m_zombies;};

        bool isAF(){if(!m_isAFSet) setIsAF();return m_isAF;};
        bool isEmpty(){return m_isEmpty;};
        std::string hforMessage(){return m_hforMessage;};
        bool noXsec(){if(!m_noXsecSet) xSec_SF(); return m_noXsec;}; 

        static void setXMLFile(const char* f);
        static DFParser getParser(){return s_parser;};
        static void setChannel(const char* channel){m_channel = channel;}

        Extractor(std::string);
        ~Extractor();

        double xSec_SF();
};
#endif
