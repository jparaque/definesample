#ifndef DFPARSER_H
#define DFPARSER_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

// Srructs needed for the parser
// Marker to determine where RN and Process names are
struct Marker{
    std::string before;
    std::string after;
};
// Add suffix if a given pattern is found
struct Subs{
    std::string id;
    std::string suffix;
};

struct Ntuple{
    std::string name;
    std::string baseHisto;
    std::string hforHisto;
    int baseHistoBin;
};

// Substitutions container
typedef std::vector<Subs> SubsCont;

class DFParser {
public:
    DFParser ();
    virtual ~DFParser ();

    int parseFile(const char* file = "config.xml");
    std::string getSamplePath(){return m_sample_path;};
    std::string getSampleID(){return m_sample_id;};
    std::string getSampleVersion(){return m_sample_version;};
    std::string getSampleSuffix(){return m_sample_suffix;};
    Marker getRunMarkers(){return m_run_markers;};
    Marker getProcMarkers(){return m_proc_markers;};
    SubsCont getProcSubs(){return m_proc_subs;};
    SubsCont getTagSubs(){return m_tag_subs;};
    Ntuple getNtuple(){return (m_ntuptype == MINI) ? m_mini : m_d3pd;};
    bool isMini(){return (m_ntuptype == MINI) ? true : false;};

    int printOut();


private:
    std::string m_sample_path;
    std::string m_sample_id;
    std::string m_sample_version;
    std::string m_sample_suffix;
    Marker m_run_markers;
    Marker m_proc_markers;
    SubsCont m_proc_subs;
    SubsCont m_tag_subs;
    Ntuple m_d3pd;
    Ntuple m_mini;

    int m_ntuptype;
    enum types{MINI,D3PD};
    std::string m_type;

};


#endif
