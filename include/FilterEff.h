#ifndef FILTEREFF_H
#define FILTEREFF_H
/************************************************************
Class to return filter efficiencies from a configuration file
defined by the user
Author: J.P. Araque
*************************************************************/    
#include <iostream>
#include <map>
#include <fstream>
#include <string>


class FilterEff {
public:
    FilterEff ();
    virtual ~FilterEff ();
    double getEff(int run);
    void setFile(std::string _file);

private:
    std::map<int,double> m_efficiency;//!< Map with efficiencies for each run number
    std::ifstream m_effFile;//!< File to read efficiencies from
};

#endif
