################################################################################
# Make file for Create_DS
################################################################################

SHELL = /bin/sh

DEFINES = -Dextname

SRC=src
INC=include
OBJ=obj
UTIL=util
RUN=run

CXX        = g++ -g
LD         = g++

ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS      = $(shell $(ROOTSYS)/bin/root-config --libs)
ROOTGLIBS  = $(shell root-config --glibs) -lMinuit -lHtml -lEG -lPhysics
TDPLIB = -L$(ROOTCOREDIR)/lib -lTopDataPreparation -lTopAnalysisBase

#CXXFLAGS   = -O2 $(ROOTCFLAGS)
CXXFLAGS   = $(ROOTCFLAGS)

LIBS	   = $(ROOTLIBS) $(TDPLIB)
GLIBS	   = $(ROOTGLIBS) 

INCLUDES = -I$(incdir) -I$(ROOTSYS)/include -I$(ROOTCOREDIR)/include -I$(INC)/

################################################################################
# Rules
################################################################################

all: $(RUN)/CreateDS 

$(OBJ)/parser.o: parser/tinyxml2.cpp
	@echo "Compiling parser"
	$(CXX) -c $< -o $@

$(OBJ)/DFParser.o: $(SRC)/DFParser.cxx $(OBJ)/parser.o
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(INCLUDES) $(OBJ)/parser.o

$(OBJ)/Filter.o: $(SRC)/FilterEff.cxx
	@echo "Compiling FilterEff"
	mkdir -p obj
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(INCLUDES)

$(OBJ)/Extractor.o: $(SRC)/Extractor.cxx
	@echo "Compiling Extractor"
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(INCLUDES)

$(RUN)/CreateDS: $(UTIL)/CreateDS.cxx $(OBJ)/Filter.o $(OBJ)/Extractor.o $(OBJ)/DFParser.o
	@echo Copying XS files
	mkdir -p data
	cp -r $(ROOTCOREDIR)/../TopDataPreparation/data/*data data/
	@echo "Compiling Create_DS"
	$(CXX) $(CXXFLAGS) -o $@ $< $(INCLUDES) \
	$(LIBS) $(OBJ)/Filter.o $(OBJ)/Extractor.o $(OBJ)/DFParser.o $(OBJ)/parser.o

$(RUN)/NewCreateDS: $(UTIL)/NewCreateDS.cxx $(OBJ)/Filter.o $(OBJ)/Extractor.o $(OBJ)/DFParser.o
	@echo Copying XS files
	mkdir -p data
	cp -r $(ROOTCOREDIR)/../TopDataPreparation/data/*data data/
	@echo "Compiling NewCreate_DS"
	$(CXX) $(CXXFLAGS) -o $@ $< $(INCLUDES) \
	$(LIBS) $(OBJ)/Filter.o $(OBJ)/Extractor.o $(OBJ)/DFParser.o $(OBJ)/parser.o

clean:
	rm -rf obj/*
