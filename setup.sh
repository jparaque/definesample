#!/bin/bash

echo "Setting up the DefineSample tool"
echo "--------------------------------"

echo "----- Updating submodules: "
git submodule init
git submodule update
echo
echo "----- Compiling DefineSample: "
make clean
make
